package czakiss;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class CreatePDF {

    public static final String DESTINATION_PATH = "resume.pdf";
    private static final int COLUMNS = 2;
    private final String TITLE = "Resume";
    private final String FIRST_NAME = "Kamil";
    private final String LAST_NAME = "Rękas";
    private final String PROFESSION = "Graphic Designer/ Java Developer";
    private final String EDUCATION = "2018-2020: IT - PWSZ in Tarnów\n"
            + "2018 - 2020: Graphic Designer - WSIiZ in Rzeszów";
    private final String SUMMARY = "Lorem ipsum dolor sit amet, consectetur adipiscing elit," +
            " sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad " +
            "minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea" +
            " commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit " +
            "esse cillum dolore eu fugiat nulla pariatur";

    public static void createPdf(String[] args) throws IOException, DocumentException {
        File file = new File(DESTINATION_PATH);
        new CreatePDF().createPdf(DESTINATION_PATH);
    }

    public void createPdf(String dest) throws IOException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(dest));

        document.open();
        document.add(createHead(TITLE));
        document.add(createTable());
        document.close();
    }

    public PdfPCell createHead(String headerText) {
        PdfPCell cell = new PdfPCell();

        Font bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
        Paragraph phrase = new Paragraph(headerText, bold);
        phrase.setAlignment(Element.ALIGN_CENTER);
        cell.addElement(phrase);

        cell.setColspan(2);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setMinimumHeight(50);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);

        return cell;
    }

    public PdfPTable createTable() {
        PdfPTable table = new PdfPTable(COLUMNS);

        // CONTENT
        table.addCell(createHead(TITLE));
        table.addCell(createCell("First Name"));
        table.addCell(createCell(FIRST_NAME));
        table.addCell(createCell("Last Name"));
        table.addCell(createCell(LAST_NAME));
        table.addCell(createCell("Profession"));
        table.addCell(createCell(PROFESSION));
        table.addCell(createCell("Education"));
        table.addCell(createCell(EDUCATION));
        table.addCell(createCell("Summary"));
        table.addCell(createCell(SUMMARY));

        return table;
    }

    public PdfPCell createCell(String txt) {
        PdfPCell cell = new PdfPCell();
        cell.addElement(new Paragraph(12, txt));
        cell.setPadding(10);

        return cell;
    }
}
